import energyusage
import pandas as pd

from imblearn.over_sampling import SMOTE
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics \
    import accuracy_score, classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB


df = pd.read_csv('Customer_Churn.csv')

df['TotalCharges'] = df['TotalCharges'] \
    .apply(lambda x: pd.to_numeric(x, errors='coerce')).dropna()

cat_features = df.drop(['customerID', 'TotalCharges', 'MonthlyCharges',
                        'SeniorCitizen', 'tenure'], axis=1)

le = preprocessing.LabelEncoder()
df_cat = cat_features.apply(le.fit_transform)
df_cat.head()

num_features = df[['customerID', 'TotalCharges', 'MonthlyCharges',
                   'SeniorCitizen', 'tenure']]
finaldf = pd.merge(num_features, df_cat, left_index=True, right_index=True)

finaldf = finaldf.dropna()
finaldf = finaldf.drop(['customerID'], axis=1)
X = finaldf.drop(['Churn'], axis=1)
y = finaldf['Churn']
X_train, X_test, y_train, y_test = \
    train_test_split(X, y, test_size=0.33, random_state=42)

oversample = SMOTE(k_neighbors=5)
X_smote, y_smote = oversample.fit_resample(X_train, y_train)
X_train, y_train = X_smote, y_smote

y_train.value_counts()

rf = RandomForestClassifier(random_state=46)
energyusage.evaluate(rf.fit, X_train, y_train, pdf=True)
rf.fit(X_train, y_train)
rf_preds = rf.predict(X_test)

nb = GaussianNB()
energyusage.evaluate(nb.fit, X_train, y_train, pdf=True)
nb.fit(X_train, y_train)
nb_preds = nb.predict(X_test)

lr = LogisticRegression(random_state=42)
energyusage.evaluate(lr.fit, X_train, y_train, pdf=True)
lr.fit(X_train, y_train)
lr_preds = lr.predict(X_test)


def evaluate_model(predictions, y_true):
    accuracy = accuracy_score(y_true, predictions)
    conf_matrix = confusion_matrix(y_true, predictions)
    class_report = classification_report(y_true, predictions)
    return accuracy, conf_matrix, class_report


rf_accuracy, rf_conf_matrix, rf_class_report = evaluate_model(rf_preds, y_test)
nb_accuracy, nb_conf_matrix, nb_class_report = evaluate_model(nb_preds, y_test)
lr_accuracy, lr_conf_matrix, lr_class_report = evaluate_model(lr_preds, y_test)

print("Random Forest Model:")
print("Accuracy:", rf_accuracy)
print("Confusion Matrix:\n", rf_conf_matrix)
print("Classification Report:\n", rf_class_report)

print("\nNaive Bayes Model:")
print("Accuracy:", nb_accuracy)
print("Confusion Matrix:\n", nb_conf_matrix)
print("Classification Report:\n", nb_class_report)

print("\nLogistic Regression Model:")
print("Accuracy:", lr_accuracy)
print("Confusion Matrix:\n", lr_conf_matrix)
print("Classification Report:\n", lr_class_report)
